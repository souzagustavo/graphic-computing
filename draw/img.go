package draw

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
)

var(
	Blue 	= color.RGBA{R:0, G:0, B:255, A:255}
	Red 	= color.RGBA{R:255, G:0, B:0, A:255}
	Green 	= color.RGBA{R:0, G:255, B:0, A:255}
)

var rect = image.Rect(0, 0, 200, 200)

type Img struct {
	img	*image.RGBA
}

func NewImg() *Img{
	 return &Img{
	 	img: image.NewRGBA(rect),
	 }
}

func (img *Img) Draw(set Set, col *color.RGBA){
	for _,p := range set.Points {
		if p.X < 0 || p.Y < 0 || col == nil{
			continue
		}
		img.img.Set(p.X, p.Y, col)
	}
}

func (img *Img) Png(filename string) {
	toImg, _ := os.Create("resources/"+filename)
	defer toImg.Close()
	err := png.Encode(toImg, img.img)
	if err != nil {
		_= fmt.Errorf("%s", err.Error())
	}
}

func (img *Img) Bytes() []byte {
	buffer := new(bytes.Buffer)
	_ = png.Encode(buffer, img.img)
	return buffer.Bytes()
}
