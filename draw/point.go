package draw

type Point struct {
	X, Y, Z int
}

type Set struct {
	Points []Point
}

func NewSet() *Set {
	return &Set{Points: []Point{}}
}

func (set *Set) Add(points ...Point) {
	for _, p := range points {
		set.Points = append(set.Points, p)
	}
}

func (set *Set) AddSet(obj Set) {
	set.Points = append(set.Points, obj.Points...)
}
