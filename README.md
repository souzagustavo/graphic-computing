# Graphic Computing

## End-points
* ### /2d/bresenham?geo={string}
* ### /2d/translation?geo={string}&dx={number}&dy={number}
* ### /2d/scale?geo={string}&dx={number}&dy={number}
* ### /2d/rotate?geo={string}&angle={number}
* ### /3d/euler?geo={string}&angle_x={number}&angle_y={number}&angle_z={number}

## Angle values
* 30, 45, 60, 90, 120, 180, 225, 270, 315

## Geometric shape values
* square, triangle, sample1