package main

import (
	cg "graphic_computing/http"
	"net/http"
)

func main(){
	_ = http.ListenAndServe(":8080", cg.Routes())
}
