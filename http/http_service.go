package http

import (
	"fmt"
	"graphic_computing/cg"
	"graphic_computing/draw"
	"net/http"
	"strconv"
)


const(
	GeoSquare 	= "square"
	GeoTriangle = "triangle"
	Sample1 	= "sample1"
	Sample2 	= "sample2"
)

func getValue(label string, r * http.Request) (string, error) {
	value := r.URL.Query().Get(label)
	if value == ""{
		return value, fmt.Errorf("query param not found: %s", label)
	}
	return value, nil
}

func getNumber(label string, r * http.Request) (int, error) {
	value, err := getValue(label, r)
	if err != nil {
		return -1, err
	}
	return strconv.Atoi(value)
}

func getFloat(label string, r * http.Request) (float64, error) {
	value, err := getValue(label, r)
	if err != nil {
		return -1, err
	}
	return strconv.ParseFloat(value, 64)
}

func getSet(label string, r * http.Request) (draw.Set, error){
	value, err := getValue(label, r)
	if err != nil {
		return *draw.NewSet(), err
	}
	return GetSet(value)
}

func getAngle(label string, r * http.Request) (float64, error) {
	value, err := getNumber(label, r)
	if err != nil {
		return -1, err
	}
	return GetAngle(value)
}

func GetSet(geo string) (draw.Set, error) {
	var err error
	var set = *draw.NewSet()
	switch geo {
	case GeoSquare:
		set.Add(draw.Square...)
	case GeoTriangle:
		set.Add(draw.Triangle...)
	case Sample1:
		set.Add(draw.Sample1...)
	default:
		err = fmt.Errorf("geo not found")
	}
	return set, err
}

func GetAngle(angle int) (float64, error){
	switch angle {
	case 30: return cg.Degree30,nil
	case 45: return cg.Degree45,nil
	case 60: return cg.Degree60,nil
	case 90: return cg.Degree90,nil
	case 120: return cg.Degree120,nil
	case 180: return cg.Degree180,nil
	case 225: return cg.Degree225,nil
	case 270: return cg.Degree270,nil
	case 315: return cg.Degree315,nil
	default: return -1, fmt.Errorf("angle not found")
	}
}
