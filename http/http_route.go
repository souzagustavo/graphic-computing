package http

import (
	"github.com/gorilla/mux"
	"net/http"
)

func Routes() http.Handler {
	router := mux.NewRouter()
	router.Methods(http.MethodOptions).HandlerFunc(setOptions)
	router.StrictSlash(true)

	router.HandleFunc("/2d/bresenham", bresenhamHandler).Methods(http.MethodGet)
	router.HandleFunc("/2d/translation", translateHandler).Methods(http.MethodGet)
	router.HandleFunc("/2d/scale", scaleHandler).Methods(http.MethodGet)
	router.HandleFunc("/2d/rotate", rotateHandler).Methods(http.MethodGet)
	router.HandleFunc("/3d/euler", eulerHandler).Methods(http.MethodGet)
	return router
}



func setOptions(w http.ResponseWriter, r * http.Request){
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.WriteHeader(http.StatusNoContent)
	return
}
