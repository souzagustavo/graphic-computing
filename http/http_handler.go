package http

import (
	"graphic_computing/cg"
	"graphic_computing/draw"
	"net/http"
	"strconv"
)

func bresenhamHandler(w http.ResponseWriter, r *http.Request) {
	geo, err := getSet("geo", r)
	if err != nil {
		response400(w, err.Error()); return
	}
	img := draw.NewImg()
	img.Draw(cg.Bresenham2dPoints(geo.Points), &draw.Red)
	img.Png("bresenham2D.png")

	response200(w, img.Bytes())
}

func eulerHandler(w http.ResponseWriter, r *http.Request) {
	geo, err := getSet("geo", r)
	if err != nil {
		response400(w, err.Error()); return
	}
	ax, err := getAngle("angle_x", r)
	if err != nil {
		ax = -1
	}
	ay, err := getAngle("angle_y", r)
	if err != nil {
		ay = -1
	}
	az, err := getAngle("angle_z", r)
	if err != nil {
		az = -1
	}
	set := cg.Euler3DPoints(geo, ax, ay, az)

	img := draw.NewImg()
	img.Draw(cg.Bresenham2dPoints(geo.Points), &draw.Red)
	img.Draw(cg.Bresenham2dPoints(set.Points), &draw.Green)
	img.Png("euler3D.png")

	response200(w, img.Bytes())
}

func rotateHandler(w http.ResponseWriter, r *http.Request) {
	geo, err := getSet("geo", r)
	if err != nil {
		response400(w, err.Error()); return
	}
	angle, err := getAngle("angle", r)
	if err != nil {
		response400(w, err.Error()); return
	}
	set := cg.Rotation2dPoints(geo, angle)

	img := draw.NewImg()
	img.Draw(cg.Bresenham2dPoints(geo.Points), &draw.Red)
	img.Draw(cg.Bresenham2dPoints(set.Points), &draw.Green)
	img.Png("rotate2D.png")

	response200(w, img.Bytes())
}

func scaleHandler(w http.ResponseWriter, r *http.Request) {
	geo, err := getSet("geo", r)
	if err != nil {
		response400(w, err.Error()); return
	}
	dx, err := getFloat("dx", r)
	if err != nil {
		response400(w, err.Error()); return
	}
	dy, err := getFloat("dy", r)
	if err != nil {
		response400(w, err.Error()); return
	}
	set := cg.Scale2DPoints( geo, dx, dy)

	img := draw.NewImg()
	img.Draw(cg.Bresenham2dPoints(geo.Points), &draw.Red)
	img.Draw(cg.Bresenham2dPoints(set.Points), &draw.Green)
	img.Png("scale2D.png")

	response200(w, img.Bytes())
}

func translateHandler(w http.ResponseWriter, r * http.Request){
	geo, err := getSet("geo", r)
	if err != nil {
		response400(w, err.Error())
		return
	}
	dx, err := getNumber("dx", r)
	if err != nil {
		response400(w, err.Error())
		return
	}
	dy, err := getNumber("dy", r)
	if err != nil {
		response400(w, err.Error())
		return
	}
	set := cg.Translation2DPoints( geo, dx, dy)

	img := draw.NewImg()
	img.Draw(cg.Bresenham2dPoints(geo.Points), &draw.Red)
	img.Draw(cg.Bresenham2dPoints(set.Points), &draw.Green)
	img.Png("translate2D.png")

	response200(w, img.Bytes())
}

func response200(w http.ResponseWriter, bytes []byte) {
	w.Header().Set("Content-Type", "image/png")
	w.Header().Set("Content-Length", strconv.Itoa(len(bytes)))
	_,_ = w.Write(bytes)
}

func response400(w http.ResponseWriter, text string){
	w.Header().Set("Content-Type", "text/plain")
	_,_ = w.Write([]byte(text))
	w.WriteHeader(http.StatusBadRequest)
}
