package cg

import (
	"fmt"
	"graphic_computing/draw"
	"math"
)

const (
	Degree30  = math.Pi / 6
	Degree45  = math.Pi / 4
	Degree60  = math.Pi / 3
	Degree90  = math.Pi / 2
	Degree120 = 2 / (3 * math.Pi)
	Degree180 = math.Pi
	Degree225 = 5 / (4 * math.Pi)
	Degree270 = 3 / (2 * math.Pi)
	Degree315 = 7 / (4 * math.Pi)
)

func Translation2D(x, y, dx, dy int) (int, int) {
	return x + dx, y + dy
}

func Scale2D(x, y int, sx, sy float64) (int, int) {
	return int(math.Round(float64(x) * sx)), int(math.Round(float64(y) * sy))
}

func Rotate2D(ox, oy, x, y int, angle float64) (int, int) {
	// translate point back to origin:
	x, y = Translation2D(x, y, -ox, -oy)

	// rotate point
	xnew := int(math.Round(float64(x)*math.Cos(angle) - float64(y)*math.Sin(angle)))
	ynew := int(math.Round(float64(x)*math.Sin(angle) + float64(y)*math.Cos(angle)))

	// translate point back:
	nx, ny := Translation2D(xnew, ynew, ox, oy)
	return nx, ny
}

//---------------------------------------------------------------------------------------------------------------------
func Translation2DPoints(set draw.Set, dx, dy int) draw.Set {
	fmt.Printf("Translate 2D:\n")
	translateSet := draw.NewSet()
	for _, p := range set.Points {
		nx, ny := Translation2D(p.X, p.Y, dx, dy)
		translateSet.Add(draw.Point{X: nx, Y: ny})
		fmt.Printf("%v, %v -> %v, %v\n", p.X, p.Y, nx, ny)
	}
	return *translateSet
}

func Scale2DPoints(set draw.Set, dx, dy float64) draw.Set {
	fmt.Printf("Scale 2D:\n")
	scaleSet := draw.NewSet()
	for _, p := range set.Points {
		nx, ny := Scale2D(p.X, p.Y, dx, dy)
		scaleSet.Add(draw.Point{X: nx, Y: ny})
		fmt.Printf("%v, %v -> %v, %v\n", p.X, p.Y, nx, ny)
	}
	return *scaleSet
}

func Rotation2dPoints(set draw.Set, angle float64) draw.Set {
	fmt.Printf("Rotate 2D:\n")
	rotateSet := draw.NewSet()
	origin := set.Points[0]

	for _, p := range set.Points {
		nx, ny := Rotate2D(origin.X, origin.Y, p.X, p.Y, angle)
		rotateSet.Add(draw.Point{X: nx, Y: ny})
		fmt.Printf("%v, %v -> %v, %v\n", p.X, p.Y, nx, ny)
	}
	return *rotateSet
}
