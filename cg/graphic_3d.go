package cg

import (
	"fmt"
	"graphic_computing/draw"
	"math"
)

func Scale3D(x, y, z int, sx, sy, sz float32) (int, int, int){
	return int(float32(x) * sx), int(float32(y) * sy), int(float32(z) * sz)
}

func Euler3D(x, y, z float64, ax, ay, az float64) (float64, float64, float64){
	var nx, ny, nz float64
	//degrees around x
	if ax != -1 {
		nx = x
		ny = (y * math.Cos(ax)) + (z * -math.Sin(ax))
		nz = (y * math.Sin(ax)) + (z * math.Cos(ax))
	}
	x, y, z = math.Round(nx), math.Round(ny), math.Round(nz)

	//degrees around y
	if ay != -1 {
		nx = (x * math.Cos(ay)) + (z * math.Sin(ay))
		ny = y
		nz = (x * -math.Sin(ay)) + (z * math.Cos(ay))
	}
	x, y, z = math.Round(nx), math.Round(ny), math.Round(nz)

	//degrees around z
	if az != -1 {
		nx = (x * math.Cos(az)) + (y * math.Sin(az))
		ny = (x * -math.Sin(az)) + (y * math.Cos(az))
		nz = z
	}
	return math.Round(nx), math.Round(ny), math.Round(nz)
}

func Euler3DPoints(set draw.Set, ax, ay, az float64) draw.Set {
	fmt.Printf("Euler 3D:\n")
	eulerSet := draw.NewSet()
	for _, p := range set.Points {
		nx, ny, nz := Euler3D(float64(p.X), float64(p.Y), float64(p.Z), ax, ay, az)
		eulerSet.Add(draw.Point{X: int(nx), Y: int(ny), Z: int(nz)})
		fmt.Printf("%v, %v, %v -> %v, %v, %v\n", p.X, p.Y, p.Z, nx, ny, nz)
	}
	return *eulerSet
}
