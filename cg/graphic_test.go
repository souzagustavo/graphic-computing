package cg

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_Scale3D(t *testing.T){
	var x, y, z = 1, 4, 3
	var sx, sy, sz float32 = 2, 0, 3

	nx, ny, nz := Scale3D(x, y, z, sx, sy, sz)

	assert.Equal(t, 2, nx)
	assert.Equal(t, 0, ny)
	assert.Equal(t, 9, nz)
}

//0,0,0
//7,0,0
//7,10,2
func Test_Euler3D(t *testing.T){

	nx, ny, nz := Euler3D(0.0, 0.0, 0.0, Degree90, -1, -1)

	assert.Equal(t, 0.0, nx)
	assert.Equal(t, 0.0, ny)
	assert.Equal(t, 0.0, nz)

	nx, ny, nz = Euler3D(7.0, 0.0, 0.0, Degree90, -1, -1)

	assert.Equal(t, 7.0, nx)
	assert.Equal(t, 0.0, ny)
	assert.Equal(t, 0.0, nz)

	nx, ny, nz = Euler3D(7.0, 10.0, 2.0, Degree90, -1, -1)

	assert.Equal(t, 7.0, nx)
	assert.Equal(t, -2.0, ny)
	assert.Equal(t, 10.0, nz)
}
