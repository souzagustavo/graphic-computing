package cg

import (
	"graphic_computing/draw"
	"math"
)

func Bresenham(x1, y1, x2, y2 int) draw.Set {
	set := draw.NewSet()
	dx := float64(x2 - x1)
	dy := float64(y2 - y1)

	isSteep := math.Abs(dy) > math.Abs(dx)
	if isSteep {
		x1, y1 = y1, x1
		x2, y2 = y2, x2
	}

	swapped := x1 > x2
	if swapped {
		x1, x2 = x2, x1
		y1, y2 = y2, y1
	}
	dx = float64(x2 - x1)
	dy = float64(y2 - y1)

	err := int(dx / 2)
	ystep := 1
	if y1 < y2 {
		ystep = 1
	} else {
		ystep = -1
	}
	y := y1

	for x := x1; x < x2+1; x++ {
		if isSteep {
			set.Add(draw.Point{X: y, Y: x})
		} else {
			set.Add(draw.Point{X: x, Y: y})
		}
		err -= int(math.Round(math.Abs(dy)))

		if err < 0 {
			y += ystep
			err += int(dx)
		}
	}
	return *set
}

func Bresenham2dPoints(points []draw.Point) draw.Set {
	set := draw.NewSet()
	//link start to end
	set.AddSet(Bresenham(points[0].X, points[0].Y, points[len(points)-1].X, points[len(points)-1].Y))
	//link all slice Points
	for i := 0; i < len(points)-1; i++ {
		set.AddSet(Bresenham(points[i].X, points[i].Y, points[i+1].X, points[i+1].Y))
	}
	return *set
}
